/*
 * 
 * Context/Background:
 * Std. OpenIDConnect Auth Provider does not support Client_Credentials grant type OAuth flow - So a custom Auth Provider has to be created
 * 
 * Solution:
 * Apex Auth Namespace provides a class named AuthProviderPluginClass [Not an interface; but an abstract class]
 * 
 * Description:
 * This class is the Custom Auth Provider.
 * Implements the Client Credentials flow which is intended for server-to-server integrations. 
 * 
 * Source:
 * https://help.salesforce.com/articleView?id=sf.sso_provider_plugin_custom.htm&type=5
 * https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/authproviderplugin.htm
 * https://github.com/bobbywhitesfdc/ApigeeAuthProvider
 * https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_class_Auth_AuthProviderPluginClass.htm#apex_class_Auth_AuthProviderPluginClass
 * 
 * Class: 
 * CustomAuthProvider [extends Auth.AuthProviderPluginClass]
 * ====================================================================================================================
 * Methods to be overridden & Client_Credentials grant type supported calls mapping:
 * ----------------------------------------------------------------------------------------------
 * getCustomMetadataType()	- Returns the custom metadata type API name for a custom OAuth-based authentication provider
 * 
 * initiate()				- Definition: Returns the URL where the user is redirected for authentication. 
 * 							- But, not applicable for Client_Credential OAuth Flow
 * 							- So, this method should return authcallback URL of Auth Provider 
 * 								- URL Format: https://{salesforce-hostname}/services/authcallback/<authprovidername>
 * 
 * handleCallback()			- Definition: Uses the authentication provider’s supported authentication protocol to return an OAuth access token.
 * 							- do Auth Callout to get Access Token
 * 
 * refresh()				- Definition: Returns a new access token, which is used to update an expired access token.
 * 							- But, not applicable for Client_Credential OAuth Flow
 * 							- So, this method should initiate() call again to retreive the new access_token
 * 
 * getUserInfo()			- Definition: Returns information from the custom authentication provider about the current user.
 * 							- As this is Server-to-Server connnection, it returns UserData with as much detail as possible (mostly nulls)
 * ====================================================================================================================
 * private methods:
 * ------------------
 * getCallbackUrl()
 * retrieveToken()
 * 
 * Inner Class:
 * ------------------
 * TokenResponse
 */
public class CustomAuthProvider extends Auth.AuthProviderPluginClass{
    
    public static final String RESOURCE_CALLBACK = '/services/authcallback/';
    public static final String DEFAULT_TOKEN_TYPE = 'Bearer';
    public static final String ENCODING_XML = 'application/x-www-form-urlencoded;charset=UTF-8';
    public static final String ENCODING_JSON = 'application/json';
    public static final String DUMMY_CODE = '999';
    public static final String DOUBLEQUOTE = '"';
    
    // This class is dependant on this Custom Metadata Type created to hold custom parameters
    public static final String CUSTOM_MDT_NAME = 'Custom_ClientCredentials_AuthProvider__mdt'; 
    public static final String CMT_FIELD_CALLBACK_URL = 'Callback_URL__c';
    public static final String CMT_FIELD_PROVIDER_NAME = 'Auth_Provider_Name__c';
    public static final String CMT_FIELD_AUTHTOKEN_URL = 'Token_Endpoint__c';
    public static final String CMT_FIELD_CLIENT_ID = 'Client_Id__c';
    public static final String CMT_FIELD_CLIENT_SECRET = 'Client_Secret__c';
    public static final String CMT_FIELD_USE_JSON = 'Use_JSON_Encoding__c';
    public static final String CMT_FIELD_SCOPE = 'Scope__c';
    
    public static final String NAMED_CREDS_TOKEN_ENDPOINT_STR = '';
    
    public static final String GRANT_TYPE_PARAM = 'grant_type';
    public static final String CLIENT_ID_PARAM = 'client_id';
    public static final String CLIENT_SECRET_PARAM = 'client_secret';
    public static final String SCOPE_PARAM = 'scope';
    public static final String GRANT_TYPE_CLIENT_CREDS = 'client_credentials';
    
    
    /**
    Added Constructor purely for debugging purposes to have visibility as to when the class
    is being instantiated.
    **/
    public CustomAuthProvider() {
        super();
        System.debug('Constructor called');
    }
    
    
    /**
    Name of custom metadata type to store this auth provider configuration fields
    This method is required by its abstract parent class.
    **/
    public String getCustomMetadataType() {
        return CUSTOM_MDT_NAME;
    } 
    
    /**
    Initiate callback. No End User authorization required in this flow so skip straight to the Token request.
    The interface requires the callback url to be defined. 
    Eg: https://test.salesforce.com/services/authcallback/<authprovidername>
    **/
    public PageReference initiate(Map<string,string> config, String stateToPropagate) {
        System.debug('initiate');
        
        final PageReference pageRef = new PageReference(getCallbackUrl(config));
        pageRef.getParameters().put('state',stateToPropagate);
        pageRef.getParameters().put('code',DUMMY_CODE); // Empirically found this is required, but unused
        System.debug(pageRef.getUrl());
        return pageRef;
    } 
    
    /**
    This method composes the callback URL automatically UNLESS it has been overridden through Configuration.
    Normally one should not override the callback URL, but it's there in case the generated URL doesn't work.
    **/
    private String getCallbackUrl(Map<string,string> config) {
        // https://{salesforce-hostname}/services/authcallback/{urlsuffix}
        //final String overrideUrl = config.get(CMT_FIELD_CALLBACK_URL);
        final String generatedUrl = URL.getSalesforceBaseUrl().toExternalForm() + RESOURCE_CALLBACK + config.get(CMT_FIELD_PROVIDER_NAME);
        
        //return String.isEmpty(overrideUrl) ? generatedUrl : overrideUrl;
        return generatedURL;
    }
    
    /**
    Handle callback (from initial loop back "code" step in the flow).
    In the Client Credentials flow, this method retrieves the access token directly.
    Required by parent class.
    Error handling here is a bit painful as the UI never displays the exception or error message 
    supplied here.  The exception is thrown for Logging/Debugging purposes only. 
    **/
    public Auth.AuthProviderTokenResponse handleCallback(Map<string,string> config, Auth.AuthProviderCallbackState state ) {
        System.debug('handleCallback');
        final TokenResponse response = retrieveToken(config);
        
        if (response.isError()) {
            throw new TokenException(response.getErrorMessage());
        }
        return new Auth.AuthProviderTokenResponse(config.get(CMT_FIELD_PROVIDER_NAME)
                                                  , response.access_token
                                                  , config.get(CMT_FIELD_CLIENT_SECRET) // No Refresh Token
                                                  , state.queryParameters.get('state'));
    } 
    
    /**
    Refresh is required by the parent class and it's used if the original Access Token has expired.
    In the Client Credentials flow, there is no Refresh token, so its implementation is exactly the
    same as the Initiate() step.
    **/
    public override Auth.OAuthRefreshResult refresh(Map<String,String> config, String refreshToken) {
        System.debug('refresh');
        final TokenResponse response = retrieveToken(config);
        return new Auth.OAuthRefreshResult(response.access_token, response.token_type);
    }
    
    
    /**
    getUserInfo is required by the Parent class, but not fully supported by this provider.
    Effectively the Client Credentials flow is only useful for Server-to-Server API integrations
    and cannot be used for other contexts such as a Registration Handler for Communities.
    **/
    public Auth.UserData getUserInfo(Map<string,string> config, Auth.AuthProviderTokenResponse response) {
        System.debug('getUserInfo-was-called');
        final TokenResponse token = retrieveToken(config);
        
        final Auth.UserData userData = new Auth.UserData(
            null // identifier
            , null // firstName
            , null // lastName
            , null // fullName
            , null // email
            , null // link
            , null // userName
            , null  //locale
            , config.get(CMT_FIELD_PROVIDER_NAME) //provider
            , null // siteLoginUrl
            , new Map<String,String>());
        
        
        return userData;
    }
    
    
    /**
    Private method that gets the Auth Token using the Client Credentials Flow.
    **/
    private TokenResponse retrieveToken(Map<String,String> config) {
        
        System.debug('retrieveToken');
        
        final Boolean useJSONEncoding = Boolean.valueOf(config.get(CMT_FIELD_USE_JSON));
        
        final HttpRequest req = new HttpRequest();
        
        final PageReference endpoint = new PageReference(NAMED_CREDS_TOKEN_ENDPOINT_STR); //NOSONAR -- Protected by RemoteSite Setting
        if (!useJSONEncoding) { // Including the Query String breaks JSON encoded OAuth
            endpoint.getParameters().put('grant_type',GRANT_TYPE_CLIENT_CREDS);            
        }
        
        // Determine whether or not to use JSON encoding
        final String encoding = useJSONEncoding ? ENCODING_JSON : ENCODING_XML;
        final String encodedParams = encodeParameters(config,encoding);
        
        System.debug('Endpoint: ' + endpoint.getUrl());
        System.debug('Content-Type:' + encoding);
        //System.debug('Body:' + encodedParams);
        
        req.setEndpoint(endpoint.getUrl());
        req.setHeader('Content-Type',encoding);
        req.setHeader('Content-Length', '0');
        req.setMethod('POST'); 
        //req.setBody(encodedParams);
        
        final HTTPResponse res = new Http().send(req); 
        
        System.debug('Token Response Status: ' + res.getStatus() + ' ' + res.getStatusCode());
        final Integer statusCode = res.getStatusCode();
        
        if ( statusCode == 200) {
            TokenResponse token =  deserializeToken(res.getBody());
            // Ensure values for key fields
            token.token_type = (token.token_type == null) ? DEFAULT_TOKEN_TYPE : token.token_type;
            return token;
            
        } else  {
            return deserializeToken(res.getBody());
        }
        
    }
    
    //deserialise response and return token
    @testVisible
    private TokenResponse deserializeToken(String responseBody) {
        
        System.debug('token response:' +responseBody);
        
        // use default parsing for everything we can.
        TokenResponse parsedResponse = (TokenResponse) System.JSON.deserialize(responseBody, TokenResponse.class);
        
        return parsedResponse;
    }
    
    /**
    Conditionally encode parameters as URL-style or JSON
    **/
    @testVisible
    private String encodeParameters(Map<String,String> config,String encoding) {
        
        // Pull out the subset of configured parameters that will be sent
        Map<String,String> params = new Map<String,String>();
        params.put(GRANT_TYPE_PARAM,GRANT_TYPE_CLIENT_CREDS);
        //params.put(CLIENT_ID_PARAM, config.get(CMT_FIELD_CLIENT_ID));
        //params.put(CLIENT_SECRET_PARAM, config.get(CMT_FIELD_CLIENT_SECRET));
        final String scope = config.get(CMT_FIELD_SCOPE);
        if (!String.isEmpty(scope)) {
            params.put(SCOPE_PARAM,scope);
        }
        
        return encoding == ENCODING_JSON ? encodeAsJSON(params) : encodeAsURL(params);
    }
    
    private String encodeAsJSON(Map<String,String> params) {
        String output = '{';
        for (String key : params.keySet()) {
            output += (output == '{' ? '' : ', ');
            output += DOUBLEQUOTE + key + DOUBLEQUOTE + ':';
            output += DOUBLEQUOTE + params.get(key) + DOUBLEQUOTE;
        }
        output += '}';
        return output;
    }
    
    private String encodeAsURL(Map<String,String> params) {
        String output = '';
        for (String key : params.keySet()) {
            output += (String.isEmpty(output) ? '' : '&');
            output += key + '=' + params.get(key);
        }
        return output;
    }
    
    /**
    Sample OAuth token Response is a JSON body like this on a Successful call
    {
        "access_token": "CWUI6kpycgz7LW6iVAsOe7pprRAv",
        "expires_in": "3599",
        "token_type": "Bearer"
    }
    
    On failure, the following structure from Apigee Edge (cloud hosted Gateway)
    {
        "fault": {
            "faultstring": "Invalid client identifier {0}",
            "detail": {
                "errorcode": "oauth.v2.InvalidClientIdentifier"
			}
        }
    }
    
    The following response class is the Union of all responses
    **/
    
    public class TokenResponse {
        public String access_token {get;set;}
        public String expires_in {get;set;}
        public String token_type {get;set;}
        
        // Apigee on premise version uses this Field for error handling
        public Fault fault {get; set;}
        
        public Boolean isError() {
            return fault != null;
        }
        
        public String getErrorMessage() {
            if (fault != null) {
                // Substitute the error code to compose
                return fault.faultString.replace('{0}',fault.detail.errorcode);
            }
            return null;
        }
    }
    
    public class Fault {
        public String faultstring {get;set;}
        public Detail detail {get;set;}
    }
    
    public class Detail {
        public String errorcode {get;set;}
    }
    
    /**
    	Custom exception type so we can wrap and rethrow
    **/
    public class TokenException extends Exception {
        
    }
}
